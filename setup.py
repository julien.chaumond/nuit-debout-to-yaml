 import os
 import sys
 from setuptools import setup
 from setuptools import find_packages
 
 def read(fname):
     path = os.path.join(os.path.dirname(__file__), fname)
     f = open(path)
     return f.read()
 
 def filter_included_modules(*m):
     modules = sum(m, [])
     if sys.version_info[0] == 2 and sys.version_info[1] <= 6:
         return modules
     included_modules = set(['argparse', 'importlib', 'sysconfig'])
     return list(set(modules) - included_modules)
 
 install_requires = read('requirements.txt').split()
 
 setup(
     name='nuit-debout-to-yaml',
     version='1.0.0',
     packages=find_packages(),
 
     author='Emmanuel Raviart',
     author_email='emmanuel.raviart@data.gouv.fr',
     description='Convert Nuit Debout catalog of services to a Git repository of YAML files.',
     long_description=read('README.md'),
     license='AGPLv3+',
     url="https://framagit.org/codegouv/nuit-debout-to-yaml/",
 
     install_requires=filter_included_modules(install_requires),
 
     classifiers=[
         'Environment :: Console',
         'Intended Audience :: Information Technology',
         'Intended Audience :: System Administrators',
         'Operating System :: POSIX :: Linux',
         'License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)',
         'Programming Language :: Python',
         'Programming Language :: Python :: 3',
         'Topic :: Utilities',
     ],
     )